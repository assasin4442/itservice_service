exports.test = (req, res, next) => {
	res.send({ success: true, message: 'test system success' });
	next();
};
exports.login = (req, res, next) => {
	let body = req.body;
	// console.log(body);
	req.getConnection(function(err, connection) {
		connection.query(
			`select * from user where username = '${body.username}' and password = '${body.password}' and role != 'close'`,
			function(err, result) {
				if (err) res.send(err);
				if (result.length > 0) {
					res.send({ success: true, result: [ { id: result[0].id, role: result[0].role } ] });
				} else {
					res.send({ success: false, message: 'username & password ไม่ถูกต้อง' });
				}
				next();
			}
		);
	});
};
exports.getuser = (req, res, next) => {
	let body = req.body;
	// console.log(body);
	req.getConnection(function(err, connection) {
		connection.query(`select * from user where id = '${body.id}'`, function(err, result) {
			if (err) res.send(err);
			res.send({ success: true, result: result });
			next();
		});
	});
};
exports.updateData = (req, res, next) => {
	let body = req.body;
	// console.log(body);
	req.getConnection(function(err, connection) {
		connection.query(
			`update user set username = '${body.username}',password = '${body.password}',fname = '${body.fname}'
			,lname= '${body.lname}',position = '${body.position}',branch = '${body.branch}',phone= '${body.phone}'
			,email = '${body.email}',line_token = '${body.line_token}' where id = '${body.id}'`,
			function(err, result) {
				if (err) res.send(err);
				res.send({ success: true, message: 'แก้ไขเสร็จสิ้น' });
				next();
			}
		);
	});
};
exports.repair = (req, res, next) => {
	let body = req.body;
	// console.log(body);
	let status = 'รอดำเนินการ';
	let type = 'แจ้งซ่อม';
	req.getConnection(function(err, connection) {
		connection.query(
			`insert into repair (uid,cid,device_problem,date,status,type,detail)
			values ('${body.uid}','${body.cid}','${body.device_problem}','${body.date}','${status}','${type}','${body.detail}')`,
			function(err, result) {
				// console.log('result', result);
				if (err) res.send(err);
				var base64Data = body.image;
				require('fs').appendFile('image/repair/' + result.insertId + '.png', base64Data, 'base64', function(
					err
				) {
					res.send({ success: true, message: 'เพิ่มรายการเสร็จสิ้น', id: result.insertId });
					next();
				});
			}
		);
	});
};
exports.request = (req, res, next) => {
	let body = req.body;
	// console.log(body);
	let status = 'รอดำเนินการ';
	let type = 'ขออุปกรณ์';
	req.getConnection(function(err, connection) {
		connection.query(
			`insert into request (uid,cid,device_problem,date,status,type,detail,amount)
			values ('${body.uid}','${body.cid}','${body.device_problem}','${body.date}','${status}','${type}','${body.detail}','${body.amount}')`,
			function(err, result) {
				if (err) res.send(err);
				res.send({ success: true, message: 'เพิ่มรายการเสร็จสิ้น', id: result.insertId });
				next();
			}
		);
	});
};
exports.getrepair = (req, res, next) => {
	let body = req.body;
	// console.log(body);
	req.getConnection(function(err, connection) {
		connection.query(
			`select *,r.id as rpid,r.status as status from repair r join classroom c on r.cid=c.id
			where uid = '${body.id}' order by date DESC`,
			function(err, result) {
				if (err) res.send(err);
				res.send({ success: true, result: result });
				next();
			}
		);
	});
};
exports.getrepair_by_id = (req, res, next) => {
	let body = req.body;
	// console.log(body);
	req.getConnection(function(err, connection) {
		connection.query(
			`select *,r.id as rpid,r.status as status from repair r join classroom c on r.cid=c.id join user u on r.uid=u.id
			where r.id = '${body.id}' order by date DESC`,
			function(err, result) {
				if (err) res.send(err);
				res.send({ success: true, result: result });
				next();
			}
		);
	});
};

exports.getrequest = (req, res, next) => {
	let body = req.body;
	// console.log(body);
	req.getConnection(function(err, connection) {
		connection.query(
			`select *,r.id as rqid,r.status as status from request r join classroom c on r.cid=c.id
		 where uid = '${body.id}' order by date DESC`,
			function(err, result) {
				if (err) res.send(err);
				res.send({ success: true, result: result });
				next();
			}
		);
	});
};
exports.getrequest_by_id = (req, res, next) => {
	let body = req.body;
	// console.log(body);
	req.getConnection(function(err, connection) {
		connection.query(
			`select *,r.id as rqid,r.status as status from request r join classroom c on r.cid=c.id join user u on r.uid=u.id
		 where r.id = '${body.id}' order by date DESC`,
			function(err, result) {
				if (err) res.send(err);
				res.send({ success: true, result: result });
				next();
			}
		);
	});
};
exports.userdelrepair = (req, res, next) => {
	let body = req.body;
	// console.log(body);
	req.getConnection(function(err, connection) {
		connection.query(`delete from repair where id='${body.id}'`, function(err, result) {
			if (err) res.send(err);
			res.send({ success: true, message: 'ลบสำเร็จ' });
			next();
		});
	});
};
exports.userdelrequest = (req, res, next) => {
	let body = req.body;
	// console.log(body);
	req.getConnection(function(err, connection) {
		connection.query(`delete from request where id='${body.id}'`, function(err, result) {
			if (err) res.send(err);
			res.send({ success: true, message: 'ลบสำเร็จ' });
			next();
		});
	});
};
exports.get_man = (req, res, next) => {
	req.getConnection(function(err, connection) {
		connection.query(`SELECT line_token FROM user where role = 'man'`, function(err, result) {
			if (err) res.send(err);
			res.send({ success: true, result: result });
			next();
		});
	});
};
exports.update_list = (req, res, next) => {
	let body = req.body;
	if (body.amount !== null) {
		req.getConnection(function(err, connection) {
			connection.query(
				`update request set cid = '${body.cid}',device_problem = '${body.device_problem}',detail = '${body.detail}'
				,amount= '${body.amount}' where id = '${body.id}'`,
				function(err, result) {
					if (err) res.send(err);
					res.send({ success: true, message: 'แก้ไขเสร็จสิ้น' });
					next();
				}
			);
		});
	} else {
		req.getConnection(function(err, connection) {
			connection.query(
				`update repair set cid = '${body.cid}',device_problem = '${body.device_problem}',detail = '${body.detail}' where id = '${body.id}'`,
				function(err, result) {
					if (err) res.send(err);
					if (!err && body.image !== '')
						require('fs').writeFile('image/repair/' + body.id + '.png', body.image, 'base64', function(
							err
						) {});
					res.send({ success: true, message: 'แก้ไขเสร็จสิ้น' });
					next();
				}
			);
		});
	}
};
