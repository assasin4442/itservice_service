const user = require('./controllers/User');
const central = require('./controllers/Central');
const man = require('./controllers/Man');
const admin = require('./controllers/Admin');

module.exports = function(app, PART) {
	//---------------------test---------------------------
	app.get(PART + '/test', user.test);
	//----------------------------------------------------------------USER------------------------------------------------------------
	app.post(PART + '/login', user.login);
	app.post(PART + '/getuser', user.getuser);
	app.post(PART + '/repair', user.repair);
	app.post(PART + '/request', user.request);
	app.post(PART + '/getrepair', user.getrepair);
	app.post(PART + '/getrepair_by_id', user.getrepair_by_id);
	app.post(PART + '/getrequest', user.getrequest);
	app.post(PART + '/getrequest_by_id', user.getrequest_by_id);
	app.post(PART + '/userdelrepair', user.userdelrepair);
	app.post(PART + '/userdelrequest', user.userdelrequest);
	app.get(PART + '/get_man', user.get_man);
	app.post(PART + '/update_list', user.update_list);
	// //----------------------------------------------------------------ADMIN-----------------------------------------------------------
	app.get(PART + '/user_all', admin.user_all);
	app.post(PART + '/del_user', admin.del_user);
	app.post(PART + '/insert_user', admin.insert_user);
	app.post(PART + '/update_user', admin.update_user);
	app.post(PART + '/insert_building', admin.insert_building);
	app.post(PART + '/del_building', admin.del_building);
	app.post(PART + '/update_building', admin.update_building);
	app.post(PART + '/del_room', admin.del_room);
	app.post(PART + '/update_room', admin.update_room);
	app.post(PART + '/insert_room', admin.insert_room);
	app.get(PART + '/tool', admin.tool);
	app.post(PART + '/update_tool', admin.update_tool);
	app.post(PART + '/insert_draw_tool', admin.insert_draw_tool);
	app.get(PART + '/draw_tool', admin.draw_tool);
	app.post(PART + '/insert_tool', admin.insert_tool);
	app.post(PART + '/del_tool', admin.del_tool);
	app.post(PART + '/update_tool_all', admin.update_tool_all);
	// //----------------------------------------------------------------MAN-------------------------------------------------------------
	app.get(PART + '/getrepair_man', man.getrepair_man);
	app.post(PART + '/getrepair_man2', man.getrepair_man2);
	app.post(PART + '/getrepair_man_success', man.getrepair_man_success);
	app.post(PART + '/getrepair_man_by_id', man.getrepair_man_by_id);
	app.get(PART + '/getrequest_man', man.getrequest_man);
	app.post(PART + '/getrequest_man2', man.getrequest_man2);
	app.post(PART + '/getrequest_man_by_id', man.getrequest_man_by_id);
	app.post(PART + '/getrequest_man_success', man.getrequest_man_success);

	// //--------------------------------------------update_repair--------------------------------
	app.post(PART + '/updatestatus', man.updatestatus);
	app.post(PART + '/updatestatus2', man.updatestatus2);
	app.post(PART + '/repair_success', man.repair_success);
	// //-------------------------------------------update_requset-----------------------------
	app.post(PART + '/updatestatus3', man.updatestatus3);
	app.post(PART + '/updatestatus4', man.updatestatus4);
	app.post(PART + '/request_success', man.request_success);
	// //------------------------------------------Tool----------------------------------------
	app.get(PART + '/gettools', man.gettools);
	app.post(PART + '/update_tools', man.update_tools);
	// //----------------------------------------------------------------------------------
	app.post(PART + '/insertdraw_repair', man.insertdraw_repair);
	app.post(PART + '/updatedraw_repair', man.updatedraw_repair);
	app.post(PART + '/deldraw_repair', man.deldraw_repair);
	app.post(PART + '/getdraw_repair', man.getdraw_repair);
	app.post(PART + '/getdraw_repair_manid', man.getdraw_repair_manid);
	//----------------------------------------------------------------------------------
	app.post(PART + '/insertdraw_request', man.insertdraw_request);
	app.post(PART + '/updatedraw_request', man.updatedraw_request);
	app.post(PART + '/deldraw_request', man.deldraw_request);
	app.post(PART + '/getdraw_request', man.getdraw_request);
	app.post(PART + '/getdraw_request_manid', man.getdraw_request_manid);
	// //----------------------------------------------------------------ส่วนกลาง---------------------------------------------------------
	app.get(PART + '/getbuilding', central.getbuilding);
	app.post(PART + '/updateData', user.updateData);
	app.get(PART + '/getclassroom', central.getclassroom);
	app.get(PART + '/gettool', central.gettool);
	app.get(PART + '/getrepair', central.getrepair);
	app.get(PART + '/getrequest', central.getrequest);
	app.get(PART + '/draw_request', central.draw_request);
	app.get(PART + '/draw_repair', central.draw_repair);
	app.post(PART + '/noti_line', central.noti_line);
	app.post(PART + '/get_user_by_id', central.get_user_by_id);
};
